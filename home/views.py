from django.shortcuts import render, redirect, get_object_or_404
from .models import About , GalleryType, Gallery, Reference, Planing, Services
from .forms import ProductForm, CategoryForm
from apps.product.models import Category, Product, ProductImage

# Create your views here.
def index(request):
    abouts = About.objects.first()
    galeri_names = GalleryType.objects.all()
    galeris = Gallery.objects.filter(is_active=True)
    category= Gallery.objects.filter(is_active=True)
    references = Reference.objects.all()

    return render(request, 'index.html', {'abouts': abouts, 'name': galeri_names, 'galeris': galeris, 'category': category, 'reference': references })

def services(request):
    service = Services.objects.first()

    return render(request, 'services.html', {'service': service})

def pricing(request):
    plans = Planing.objects.filter(is_active=True)

    return render(request, 'pricing.html', {'plans': plans})

def login(request):
    return render(request, 'login.html')

def register(request):
    return render(request, 'register.html')

def admin(request):
  context = {
    'segment'  : 'index',
  }
  return render(request, "admin/pages/index.html", context)

def dashboard(request):
  return render(request, "admin/pages/index.html")


def productadd(request):
    if request.method == 'POST':
        form = ProductForm(request.POST)
        if form.is_valid():
            form.save()  # Formu kaydet
            return redirect('products')  # İşlem başarılıysa yönlendirme
    else:
        form = ProductForm()

    return render(request, 'admin/pages/productadd.html', {'form': form})

def categoryadd(request):
    if request.method == 'POST':
        form = CategoryForm(request.POST)
        if form.is_valid():
            form.save()  # Formu kaydet
            return redirect('categories')  # İşlem başarılıysa yönlendirme
    else:
        form = CategoryForm()

    return render(request, 'admin/pages/categoryadd.html', {'form': form})

def products(request):
    products = Product.objects.filter(is_active=True)

    return render(request, 'admin/pages/products.html', {'products': products})

def categories(request):
    categories = Category.objects.filter(is_active=True)

    return render(request, 'admin/pages/categories.html', {'categories': categories})

def editproduct(request, product_id):
    product = get_object_or_404(Product, id=product_id)
    # Düzenleme ekranını göstermek için product objesini şablonla birleştirin ve gönderin.
    return render(request, 'admin/pages/editproduct.html', {'product': product})
