from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('services/', views.services, name='services'),
    path('pricing/', views.pricing, name='pricing'),
    path('login/', views.login, name='login'),
    path('register/', views.register, name='register'),
    path('company/admin/', views.admin, name='admin'),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('productadd/', views.productadd, name='productadd'),
    path('categoryadd/', views.categoryadd, name='categoryadd'),
    path('products', views.products, name='products'),
    path('categories', views.categories, name='categories'),
    path('product/<int:product_id>/edit/', views.editproduct, name='editproduct'),
]