# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from apps.company.models import Company

# Create your models here.

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    token = models.CharField(
        max_length=8,
        verbose_name=("Token"),
        null=True, blank=True,
    )
    name = models.CharField ( max_length = 150,verbose_name='Ad',null=True)
    surname = models.CharField ( max_length = 150,verbose_name='Soyad',null=True)
    date_of_birth = models.DateTimeField(auto_now_add=True,verbose_name='Yaş',null=True)
    image = models.ImageField(null=True,verbose_name='Profil Resmi',upload_to='static/images')
    gender = models.CharField (max_length = 30,verbose_name='gender ',null=True)
    is_active = models.BooleanField(default=True,verbose_name='Aktif mi')
    is_cancelled = models.BooleanField(default=False,verbose_name='İptal mi')
    slug = models.SlugField(
        max_length=50,
        null=True, blank=True
    )

    company = models.ForeignKey(
        Company,
        null=True, blank=True,
        on_delete=models.CASCADE
    )

    def __unicode__(self):
        return "{}".format(self.name)

    def __str__(self):
        return self.__unicode__()
    