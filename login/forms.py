# -*- coding: utf-8 -*-

from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.models import User



class LoginForm(forms.Form):
    username = forms.CharField(max_length=100, widget=forms.TextInput(attrs={"class":"form-control form-control-lg","placeholder":"KULLANICI ADI", "style":"display:inline; width:90%;"}))
    password = forms.CharField(max_length=100, widget=forms.PasswordInput(attrs={"class":"form-control form-control-lg","placeholder":"SIFRE", "style":"display:inline; width:90%;"}))

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        if username and password:
            user = authenticate(username=username, password=password)
            if not user:
                raise forms.ValidationError("Kullanıcı adını veya şifreyi yanlış girdiniz!")
        return super(LoginForm, self).clean()

class RegisterForm(forms.ModelForm):
    username = forms.CharField(max_length=100, label='Kullanıcı Adı', widget=forms.TextInput(attrs={"class":"form-control","placeholder":"KULLANICI ADI", "style":"display:inline; width:90%;"}))
    email = forms.CharField(max_length=100, label='Email', widget=forms.TextInput(attrs={"class":"form-control","placeholder":"E-POSTA", "style":"display:inline; width:90%;"}))
    password1 = forms.CharField(max_length=100, label='Parola', widget=forms.PasswordInput(attrs={"class":"form-control","placeholder":"SIFRE", "style":"display:inline; width:90%;"}))
    password2 = forms.CharField(max_length=100, label='Parola Doğrulama', widget=forms.PasswordInput(attrs={"class":"form-control","placeholder":"SIFRE TEKRAR", "style":"display:inline; width:90%;"}))
    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password1',
            'password2',
        ]

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            print("Şifreler eşleşmiyor!")
            raise forms.ValidationError("Kullanıcı adını veya şifreyi yanlış girdiniz!")
        return super(RegisterForm, self).clean()



