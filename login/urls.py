from django.urls import path , include

from .views import *
app_name = "login"

urlpatterns = [
    path('giris/', login_view, name='login'),
    path('kayit/ol/', register_view, name="register"),
    path('cikis/', logout_view, name="logout")
]