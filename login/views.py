# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import LoginForm, RegisterForm
from .models import UserProfile

def login_view(request):
    form = LoginForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        login(request, user)
        return redirect('/dashboard/')
    return render(request, "login.html", {"form": form})

def register_view(request):
    form = RegisterForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        password = form.cleaned_data.get('password1')
        user.set_password(password)
        # user.is_staff = user.is_superuser = True
        user.save()
        new_user = authenticate(username=user.username, password=password)
        login(request, new_user)
        user_profile = UserProfile()
        user_profile.user = user
        user_profile.save()
        # Kullanici aktivasyon su anlik pasif
        # try:
        #     if 1==1:    
        #         code = random.randint(100000,999999)
        #         mail = """                                                                                                                              
        #             Merhaba {username} ! Turumav sitesi kayıt aktivasyon kodunuz : {code}                                                                                                                    
        #         """.format(username=user.username,
        #                     code=code)  
        #         email = EmailMessage('Subject', mail, to=[str(user.email)]) 
        #         email.send()
        #         user_profile.token = code
        #         user_profile.save()
        
        # except:
        #     print("Hata:")
        #     logout(request)
        return redirect('/')
    return render(request, "register.html", {"form": form, 'title': 'Üye Ol'}) 

def logout_view(request):
    logout(request)
    return redirect('/')
