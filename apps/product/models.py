from django.db import models
from apps.company.models import Company, Branch
# Create your models here.


class Category(models.Model):
    name = models.CharField(
        max_length=128, 
        null=True, 
        blank=True,
        verbose_name="Name"
    )

    branch = models.ForeignKey(
        Branch,
        null=True, blank=True,
        on_delete=models.CASCADE
    )
    
    created_at = models.DateTimeField(
        auto_now_add=True
    )

    is_active = models.BooleanField(default=True, verbose_name='Aktif mi')

    def __unicode__(self):
        return "{}".format(self.name)

    def __str__(self):
        return self.__unicode__()


class Language(models.Model):
    name = models.CharField(
        max_length=64, 
        null=True, 
        blank=True,
        verbose_name="Name"
    )

    created_at = models.DateTimeField(
        auto_now_add=True
    )

    is_active = models.BooleanField(default=True, verbose_name='Aktif mi')

    def __unicode__(self):
        return "{}".format(self.name)

    def __str__(self):
        return self.__unicode__()
    

class Product(models.Model):
    code = models.CharField(
        max_length=264, 
        null=True, 
        blank=True,
        verbose_name="Code"
    )

    name = models.CharField(
        max_length=264, 
        null=True, 
        blank=True,
        verbose_name="Name"
    )

    branch = models.ForeignKey(
        Branch,
        null=True, blank=True,
        on_delete=models.CASCADE
    )

    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    price = models.FloatField(null=True, blank=True, verbose_name="Price")
    deci = models.FloatField(null=True, blank=True, verbose_name="Deci")

    description = models.TextField(
        max_length=1024, 
        null=True, 
        blank=True,
        verbose_name="Description"
    )

    is_active = models.BooleanField(default=True,verbose_name='Aktif mi')
    is_cancelled = models.BooleanField(default=False,verbose_name='İptal mi')

    created_by = models.ForeignKey(
        "auth.User",
        null=True, blank=True,
        on_delete=models.CASCADE
    )
    created_at = models.DateTimeField(
        auto_now_add=True
    )

    def __unicode__(self):
        return "{}".format(self.name)

    def __str__(self):
        return self.__unicode__()
    

class ProductImage(models.Model):
    product = models.ForeignKey(
        Product, 
        on_delete=models.PROTECT, 
        null=True, 
        blank=True
    )
    image_url = models.CharField(
        max_length=128, 
        null=True, 
        blank=True,
        verbose_name="Image Url"
    )

    image_base64 = models.CharField(
        null=True, 
        blank=True,
        verbose_name="Image Base64",
        max_length=1024 
    )

    created_at = models.DateTimeField(
        auto_now_add=True
    )

    is_active = models.BooleanField(default=True, verbose_name='Aktif mi')

    def __unicode__(self):
        return "{}".format(self.name)

    def __str__(self):
        return self.__unicode__()
    

class ProductLanguage(models.Model):
    language = models.ForeignKey(
        Language,
        null=True, blank=True,
        on_delete=models.CASCADE
    )

    name = models.CharField(
        max_length=64, 
        null=True, 
        blank=True,
        verbose_name="Name"
    )

    product = models.ForeignKey(
        Product,
        on_delete=models.PROTECT,
        null=True, blank=True
    )

    created_at = models.DateTimeField(
        auto_now_add=True
    )

    is_active = models.BooleanField(default=True, verbose_name='Aktif mi')

    def __unicode__(self):
        return "{}".format(self.name)

    def __str__(self):
        return self.__unicode__()