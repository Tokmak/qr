from django.contrib import admin

# Register your models here.
from django.contrib import admin

# Register your models here.

from django.contrib import admin

from apps.product.models import *

class CategoryAdmin(admin.ModelAdmin):
    list_display = ["name", "is_active"]
    search_fields = ['name']
    raw_id_fields = ["branch"]

class ProductLanguageAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_display = ["name", "is_active"]
    raw_id_fields = ["product"]

class ProductAdmin(admin.ModelAdmin):
    list_display = ["code", "name", "price", "is_active"]
    search_fields = ['name']
    raw_id_fields = ["category", "branch", "created_by"]

admin.site.register(Category, CategoryAdmin)
admin.site.register(ProductLanguage, ProductLanguageAdmin)
admin.site.register(Product, ProductAdmin)

