from django.db import models


class Company(models.Model):
    title = models.CharField(
        max_length=264, 
        null=True, 
        blank=True,
        verbose_name="Name"
    )

    is_active = models.BooleanField(
        default=True,
        verbose_name='Aktif mi'
    )

    is_cancelled = models.BooleanField(
        default=False,
        verbose_name='İptal mi'
    )

    created_at = models.DateTimeField(
        auto_now_add=True
    )

    def __unicode__(self):
        return "{}".format(self.title)

    def __str__(self):
        return self.__unicode__()
    

class Branch(models.Model):
    name = models.CharField(
        max_length=264, 
        null=True, 
        blank=True,
        verbose_name="Name"
    )

    company = models.ForeignKey(
        Company,
        null=True, blank=True,
        on_delete=models.CASCADE
    )

    is_active = models.BooleanField(
        default=True,
        verbose_name='Aktif mi'
    )

    is_cancelled = models.BooleanField(
        default=False,
        verbose_name='İptal mi'
    )

    created_at = models.DateTimeField(
        auto_now_add=True
    )

    def __unicode__(self):
        return "{}".format(self.name)

    def __str__(self):
        return self.__unicode__()