from django.contrib import admin

# Register your models here.

from django.contrib import admin

from apps.company.models import *

class CompanyAdmin(admin.ModelAdmin):
    list_display = ["title", "is_active"]
    search_fields = ['title']

class BranchAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_display = ["name", "is_active"]
    raw_id_fields = ["company"]

admin.site.register(Company, CompanyAdmin)
admin.site.register(Branch, BranchAdmin)
